//DATOS DE PRUEBA
var ropa = [{text:'Camisas',value:'camisas'},{text:'Patalones',value:'pantalones'}]
var comida = [{text:'Frutas',value:'frutas'},{text:'Cereales',value:'cereales'}];
    
var camisas = [{text:'Camisa1',value:'camisa1',sold:{jan:2,feb:5,mar:10,apr:15}},
                {text:'Camisa2',value:'camisa2',sold:{jan:8,feb:15,mar:20,apr:4}},
                {text:'Camisa3',value:'camisa3',sold:{jan:3,feb:6,mar:9,apr:0}}]

var pantalones = [{text:'Pantalón1',value:'pantalon1',sold:{jan:2,feb:5,mar:10,apr:15}},
                {text:'Pantalón2',value:'pantalon2',sold:{jan:1,feb:0,mar:20,apr:5}},
                {text:'Pantalón3',value:'pantalon3',sold:{jan:2,feb:5,mar:10,apr:15}}]

var frutas = [{text:'Fruta1',value:'fruta1',sold:{jan:20,feb:15,mar:7,apr:1}},
                {text:'Fruta2',value:'fruta2',sold:{jan:20,feb:15,mar:10,apr:15}},
                {text:'Fruta3',value:'fruta3',sold:{jan:12,feb:15,mar:10,apr:5}}]

var cereales = [{text:'Cereal1',value:'cereal1',sold:{jan:1,feb:5,mar:0,apr:15}},
                {text:'Cereal2',value:'cereal2',sold:{jan:2,feb:10,mar:1,apr:20}},
                {text:'Cereal3',value:'cereal3',sold:{jan:3,feb:15,mar:10,apr:15}}]

var currentArray = []



//Funciones
function main(){
  changeCategory()
}

function changeCategory(){
  var category = document.getElementById('category')
  var selectedValue = category.options[category.selectedIndex].value;
  if (selectedValue=='ropa') {
    fillProduct(ropa)
  }
  else
    fillProduct(comida)
}

function fillProduct(newElements){
  var selectProduct = document.getElementById('product')
  if(selectProduct.firstChild!=null)
    selectProduct.innerHTML = ''
  for (var i = 0; i < newElements.length; i++) {
    selectProduct.innerHTML += `<option value=${newElements[i].value}>${newElements[i].text}</option>` 
  } 
  $('select').material_select()
  changeProduct()
}

function changeProduct(){
  var product = document.getElementById('product')
  var selectedValue = product.options[product.selectedIndex].value;
  if (selectedValue=='camisas') {
    fillBrand(camisas)
  }
  else if (selectedValue=='pantalones') {
    fillBrand(pantalones)
  }
  else if (selectedValue=='frutas') {
    fillBrand(frutas)
  }
  else
    fillBrand(cereales)
}

function fillBrand(newElements){
  var selectProduct = document.getElementById('brand')
  if(selectProduct.firstChild!=null)
    selectProduct.innerHTML = ''
  for (var i = 0; i < newElements.length; i++) {
    selectProduct.innerHTML += `<option value=${newElements[i].value}>${newElements[i].text}</option>` 
  } 
  $('select').material_select()
  currentArray = newElements
  changeGraph()
}

function changeGraph(){
  var brand = document.getElementById('brand')
  var selectedValue = brand.options[brand.selectedIndex].value;
  var ventas = {}
  for (var i = 0; i < currentArray.length; i++) {
    if(currentArray[i].value==selectedValue)
      ventas = currentArray[i].sold
  }


  // Create the chart
  Highcharts.chart('container', {
    chart: {
            type: 'column'
        },
        title: {
            text: 'Ventas 2018'
        },
        xAxis: {
            categories: ['Enero', 'Febrero', 'Marzo','Abril'],
            title:{
              text: 'Meses'
            }
        },
        yAxis: {
            title: {
                text: 'Ventas'
            }
        },
        series: [
        {
            "name": "Ventas",
            "data": [
                {
                    "name": "Enero",
                    "y": ventas.jan
                },
                {
                    "name": "Febrero",
                    "y": ventas.feb
                },
                {
                    "name": "Marzo",
                    "y": ventas.mar
                },
                {
                    "name": "Abril",
                    "y": ventas.apr
                }
            ]
        }
    ],
    });
}